# Coding ABSTRACT to sharing, can follow issues or other (don't forget sharing your code!!!thanx!!!), to create JavaScript vanilla carousel

- create a JS object array `slides` to give picture informations (keys, values)
- install a even listener `#toolbar-toggle`
- create a global variable `state` : an object with carousel state, if this is turning ou give position

# Coding ABSTRACT traduction french to sharing

2. Dans le javascript, créer un tableau `slides` contenant les informations de chacune des images sous la forme d'un objet ayant pour propriété :
   - image : le src de l'image depuis le HTML
   - legend : le titre qui sera affiché sous l'image
3. Installer un gestionnaire d'événement sur `#toolbar-toggle` pour qu'au clic, cela affiche la UL contenant les différents boutons de contrôle.
4. Créer une variable globale `state` qui sera un objet qui contiendra les informations sur l'état du carrousel (à quelle image on est, si le diapo est lancé)
5. Initialiser le carrousel. Attention, on va s'assurer que le code s'éxécute bien uniquement une fois que la page HTML est chargée [Déclencher du code une fois la page HTML chargée](https://www.notion.so/D-clencher-du-code-une-fois-la-page-HTML-charg-e-a8e0cbb5bdd1494d891c27e75af4778a). Dans cette fonction anonyme, on va initialiser l'état du carrousel en indiquant que la première photo à afficher est celle à l'index 0 `state.index = 0;`
6. On va créer une fonction `refreshSlider()` qui se charge d'afficher une image (ici la 1ère) et qui sera appelée dès l'initialisation.
   Cette fonction se charge de cibler l'image et le figcaption, et de modifier respectivement le src et le texte.
   _Un figcaption est une balise qui permet d'ajouter une légende à une image ou un schéma contenu dans un `figure`_
