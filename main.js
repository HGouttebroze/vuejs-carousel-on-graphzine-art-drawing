import "./style.css";
import "./base.css";
import "./normalize.css";

let state;

let slides = [
  {
    image: "https://robohash.org/xp?set=set4",
    description: "You got to fight! For your right!",
  },
  {
    image: "https://robohash.org/xp1?set=set4",
    description: "You got to fight tonight! Fight For your right tonight!",
  },
  {
    image: "https://robohash.org/xp2?set=set4",
    description:
      "You got to fight! For your right! Yeh! You got to fight tonight! Fight For your right tonight!",
  },
  { image: "images/1.jpg", description: "Crumb paint" },
  { image: "images/2.jpg", description: "Crumb paint 1" },

  { image: "images/3.jpg", description: "Crumb paint 2" },

  { image: "images/4.jpg", description: "Crumb paint 3" },

  { image: "images/5.jpg", description: "Crumb paint 4" },

  { image: "images/6.jpg", description: "Crumb paint" },

  { image: "images/7.jpg", description: "Crumb paint" },

  { image: "images/9.jpg", description: "Crumb paint" },
];

console.log(slides);
console.table(slides);
document.querySelector("#app").innerHTML = `
  <div class="img--crumb">
    <img src="./images/bg2.jpg" alt="Crumb paint">
  </div>
  <header>
    <h1>Slider DIY en JavaScript Vanilla ! Dessin de Robert Crumb</h1>
    <nav class="toolbar">
      <a id="toolbar-toggle" href="#"><i class="fa fa-arrow-right"></i> Barre d'outils</a>
      <ul class="hide">
        <li><button id="slider-previous" title="Image précédente"><i class="fa fa-backward"></i></button></li>
        <li><button id="slider-toggle" title="Démarrer le carrousel"><i class="fa fa-play"></i></button></li>
        <li><button id="slider-next" title="Image suivante"><i class="fa fa-forward"></i></button></li>
        <li><button id="slider-random" title="Sélectionner une image aléatoire"><i class="fa fa-random"></i></button>
        </li>
      </ul>
    </nav>
  </header>

  <main>
    <figure id="slider" class="slider">
      <img src="" alt="Photo du carrousel">
      <figcaption></figcaption>
    </figure>
  </main>
  <a https://gitlab.com/HGouttebroze/vuejs-carousel-on-graphzine-art-drawing/" target="_blank">GitLab Code Source</a>
`;

function refreshSlider() {
  let sliderImage;
  let sliderDescription;
  // check content tags
  sliderImage = document.querySelector("#slider img");
  sliderDescription = document.querySelector("#slider figcaption");
  // change picture src & desc
  sliderImage.src = slides[state.index].image;
  sliderDescription.textContent = slides[state.index].description;
  console.log(sliderImage);
}

document.addEventListener("DOMContentLoaded", function () {
  state = new Object();
  state.index = 0;
  // call function
  refreshSlider();
});
